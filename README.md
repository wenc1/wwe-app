# WWE APP

## 1. Project Overview
WWE APP is an application for reading an biography about you favorite WWE Superstar.

## 2. Main Technologies:
PHP, Laravel, JavaScript, Node.js, React Native, Expo, Git, Firebase, Firebase Console, Cloud Messaging, Facebook-API

## 3. Prerequisites for Starting the Project:
- Node.js
- VScode
- Git
- XAMPP
- Composer

## 4. Setting Up the BackEnd Server of the Project
  1. Clone this Repository locally and pull it's contents
  2. Find your IPV4 from cmd with a command ```ipconfig```
  3. Open the ```\BackEnd-Laravel``` folder in VScode and in the terminal run ```npm install```
  4. Run  ```php artisan serve --host=(your IPV4) --port=8000 ```
 
## 5. Setting Up The FrondEnd of the project
  1. Open the ```\FrontEnd-ReactNative``` folder in VScode and in the terminal run ```npm install```
  3. In folder ```\FrontEnd-ReactNative\Constants``` open Constansts.js and change URL with ```\http://(your IPV4)/api/roster```
  2. To start the app run ```npm start``` - within a few seconds you should see the app open a new tab in your browser

## 6. Start the Aplication
  1. Download Expo on your Mobile Device and scan the QR code
  2. In FrontEnd-ReactNatve's Terminal run build:android, then in your Expo accont you can find an appk file, which you can install on your Android Device

## 7. Usage:
  1. Firts you can login with your Facebook account

 <a href="https://ibb.co/kSMX18y"><img width="300" src="https://i.ibb.co/qrxp5sn/Login-Page.png" alt="Home-Page-1" border="0"></a>

  2. Then you are on Home screen, there are some WWE superstars

<a href="https://ibb.co/x3r7ZC9"><img width="300" src="https://i.ibb.co/cxsrqgf/Home-Page-1.jpg" alt="Home-Page-1" border="0"></a>
<br></br>
<a href="https://ibb.co/cTq0Ggm"><img width="300" src="https://i.ibb.co/DKdhxVq/Home-Page.png" alt="Home-Page" border="0"></a>

  3. Click on your favourite superstar and you can read the biography about him and listen his Theme song.

<a href="https://ibb.co/s6YrVGr"><img width="300" src="https://i.ibb.co/8dZV7pV/Single-Wrestler.png" alt="Single-Wrestler" border="0"></a>

  <a href="https://ibb.co/H4SjysZ"><img width="300" src="https://i.ibb.co/LRF3yWK/Single-Wrestler-1.jpg" alt="Single-Wrestler-1" border="0"></a>

<a href="https://ibb.co/7GBJWCD"><img width="300" src="https://i.ibb.co/HrRDz4y/Single-Wrestler-4.png" alt="Single-Wrestler-4" border="0"></a>

<a href="https://ibb.co/rsgtPGt"><img width="300" src="https://i.ibb.co/FV9WvbW/Single-Wrestler-5.png" alt="Single-Wrestler-5" border="0"></a>

 4. After installing the app on your Android device you can recive notifications from my Firebase Console.

<a href="https://ibb.co/HBSYs8G"><img idth="600" src="https://i.ibb.co/9cD2L5y/Notifications.png" alt="Notifications" border="0"></a>

<a href="https://ibb.co/p19cbF0"><img width="300" src="https://i.ibb.co/K53dbv0/Notifications1.png" alt="Notifications1" border="0"></a>