import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Clipboard, Alert} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Home from './Containers/Home.js';
import LogInPage from './Containers/LogInPage.js';
import AuthContext from './Containers/AuthContext.js'
import SingleWrestler from './Containers/SingleWrestler.js';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';

const Stack = createStackNavigator();

export default function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: false,
    userData: null,
  });

  // useEffect(() => {
  //   token = (await Notifications.getExpoPushTokenAsync()).data;
  //   Clipboard.setString(token)
  //   Alert.alert("Device token is copied to clipboard!", token);

  // }, []);

  return (
    <AuthContext.Provider value={{ ...authValue, setAuth: setAuthValue }}>
      {authValue.isLoggedIn ?
        <NavigationContainer style={styles.container}>
          <Stack.Navigator screenOptions={{
            headerStyle: {
              backgroundColor: "#4267B2"
            },
            headerTintColor: "#FFFFFF",
            headerTitleStyle: {
              fontSize: 20,
            }
          }}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="SingleWrestler" component={SingleWrestler} />
          </Stack.Navigator>
        </NavigationContainer>
        :
        <View style={styles.container}>
          <LogInPage></LogInPage>
        </View>
      }
    </AuthContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
