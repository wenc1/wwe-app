import { createContext } from 'react';

const AuthContext = createContext({
  userData: null,
  isLoggedIn: false,
  setAuth: () => {},
});

export default AuthContext;
