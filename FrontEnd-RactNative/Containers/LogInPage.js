import React, { useContext } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import * as Facebook from "expo-facebook";
import AuthContext from './AuthContext.js'
import imageLogo from "../Pictures/wweLogo.png"
import { TouchableOpacity } from "react-native-gesture-handler";

const LogInPage = () => {
    const {setAuth } = useContext(AuthContext);

    async function logIn() {
        try {
            await Facebook.initializeAsync("429713141705351");

            const { type, token } = await Facebook.logInWithReadPermissionsAsync({
                permissions: ["public_profile"],
            });

            if (type === "success") {
                const response = await fetch(
                    `https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`
                );
                const data = await response.json();
                const auth = {
                    userData: data,
                    isLoggedIn: true
                }
                setAuth(auth);   
            }
        } 
        catch ({ message }) {
            alert(`Facebook Login Error: ${message}`);
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Image source={imageLogo} style={styles.logo} />
                <Text style={styles.logoText}> Superstars Biography</Text>
            </View>
            <TouchableOpacity onPress={() => logIn()} style={styles.btnLogin}>
                <Text style={styles.text} >Login with Facebook</Text>
            </TouchableOpacity>
        </View>
    );
};

export default LogInPage;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        alignItems: "center",
    },
    btnLogin: {
        width: 200,
        height: 45,
        borderRadius: 25,
        backgroundColor: "#4267B2",
        justifyContent: 'center',
        marginTop: 20
    },
    text: {
        color: 'rgba(255, 255, 255, 0.7)',
        fontSize: 16,
        textAlign: 'center'
    },
    logoContainer: {
        alignItems: 'center',
        marginTop:200
    },
    logo: {
        width: 220,
        height: 220
    },
    logoText: {
        paddingBottom: 20,
        paddingTop: 30,
        color: 'black',
        fontSize: 30
    },
});