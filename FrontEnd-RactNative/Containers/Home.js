import React, { useContext, useEffect, useState } from 'react';
import { FlatList, StyleSheet, SafeAreaView, Text, Dimensions, TouchableOpacity, View } from 'react-native';
import AuthContext from './AuthContext';
import WrestlerHome from '../Components/HomePage/WrestlerHome';
import { URL } from '../Constants/Constants';

const Home = ({ navigation }) => {
    const [roster, setRoster] = useState([]);
    const { setAuth } = useContext(AuthContext);

    useEffect(() => {
        fetch(`${URL}`)
            .then(response => response.json())
            .then(data => setRoster(data.result))
            .catch(error => new Error('error'))
    }, []);

    const logOut = () => {
        setAuth({
            userData: null,
            isLoggedIn: false
        });
    };

    const renderItem = ({ item }) => {
        return (
            <WrestlerHome item={item} navigation={navigation}></WrestlerHome>
        );
    };

    return (
        <SafeAreaView style={styles.container}>
            <TouchableOpacity onPress={() => logOut()} style={styles.btnLogin}>
                <Text style={styles.textBtn} >Log Out</Text>
            </TouchableOpacity>
            <Text style={styles.title} >Superstars</Text>
            <FlatList
                numColumns={2}
                style={styles.container}
                data={roster}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
            />
        </SafeAreaView>
    );
};

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: Dimensions.get('window').width
    },
    btnLogin: {
        width: 150,
        height: 30,
        borderRadius: 25,
        backgroundColor: "#4267B2",
        justifyContent: 'center',
        marginTop: 10,
        marginLeft: Dimensions.get('window').width / 2 - 75,
        marginBottom: 5,
    },
    textBtn: {
        color: 'rgba(255, 255, 255, 0.7)',
        fontSize: 16,
        textAlign: 'center'
    },
    title: {
        textAlign: 'center',
        paddingBottom: 10,
        paddingTop: 5,
        color: 'black',
        fontSize: 40,
    }
});
