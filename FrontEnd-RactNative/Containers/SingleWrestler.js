import React, { useState, useEffect, useContext } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, ScrollView, } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import AuthContext from './AuthContext';
import YoutubePlayer from "react-native-youtube-iframe";
import YouTubeLogo from "../Pictures/YouTubeLogo.png";
import { URL } from '../Constants/Constants';

const SingleWrestler = ({ route }) => {

    const { itemId } = route.params;
    const { setAuth } = useContext(AuthContext);
    const [wrestler, setWrestler] = useState([]);

    const logOut = () => {
        setAuth({
            userData: null,
            isLoggedIn: false
        });
    };

    useEffect(() => {
        fetch(`${URL}/${itemId}`)
            .then(response => response.json())
            .then(data => { setWrestler(data.result) })
            .catch(error => new Error('error'))
    }, [itemId]);

    return (
        <ScrollView style={styles.container}>
            <TouchableOpacity onPress={() => logOut()} style={styles.btnLogin}>
                <Text style={styles.textBtn} >Log Out</Text>
            </TouchableOpacity>
            <Text style={styles.title} >{wrestler.name}</Text>
            <Image
                style={styles.image}
                resizeMode="contain"
                source={{ uri: `${wrestler.fullPicture}` }}
            />
            <Text style={styles.bioText} >Biography</Text>
            <Text style={styles.bio} >{wrestler.bio}</Text>
            <View style={styles.border} />
            <View style={styles.logoContainer}>
                <Image source={YouTubeLogo} style={styles.logo} />
            </View>
            <Text style={styles.titantron} >Titantron</Text>
            <View style={styles.player}>
                <YoutubePlayer
                    height={(Dimensions.get('window').width - 200)}
                    width={Dimensions.get('window').width - 80}
                    videoId={wrestler.youtubeLink}
                />
            </View>
        </ScrollView>
    );
};

export default SingleWrestler;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    btnLogin: {
        width: 150,
        height: 30,
        borderRadius: 25,
        backgroundColor: "#4267B2",
        justifyContent: 'center',
        marginTop: 10,
        marginLeft: Dimensions.get('window').width / 2 - 75,
        marginBottom: 5,
    },
    border: {
        marginLeft: 30,
        marginRight: 30,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    textBtn: {
        color: 'rgba(255, 255, 255, 0.7)',
        fontSize: 16,
        textAlign: 'center'
    },
    title: {
        fontWeight: 'bold',
        paddingBottom: 5,
        paddingTop: 5,
        color: 'black',
        fontSize: 30,
        textAlign: 'center',
    },
    bio: {
        paddingBottom: 20,
        paddingRight: 20,
        paddingLeft: 20,
        fontSize: 15,
        alignItems: 'center',
        textAlign: 'center',
    },
    image: {
        marginTop: 5,
        marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 2.25,
        shadowRadius: 3.84,
        alignItems: 'center',
        height: 200
    },
    player: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 2.25,
        shadowRadius: 3.84,
        alignItems: 'center',
        justifyContent: 'center',

    },
    titantron: {
        paddingTop: 5,
        paddingBottom: 10,
        color: 'black',
        fontSize: 30,
        textAlign: 'center',
    },
    bioText: {
        paddingBottom: 10,
        color: 'black',
        fontSize: 30,
        textAlign: 'center',
    },
    logoContainer: {
        alignItems: 'center',
        marginTop: 15
    },
    logo: {
        width: 72,
        height: 30
    },
});