import { View, Image, StyleSheet, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import React from 'react';

const WrestlerHome = ({ item, navigation }) => {
    return (
        <View key={item.id} style={styles.item}>
            <TouchableOpacity onPress={() => navigation.navigate('SingleWrestler', { itemId: item.id })}>
                <Image
                    style={styles.image}
                    resizeMode="contain"
                    source={{ uri: `${item.picture}` }}
                />
                <Text style={styles.text}>{item.name}</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    item: {
        padding: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 8,
        marginHorizontal: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    image: {
        width: 120,
        height: 120,
    },
    text: {
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16

    },
});

export default WrestlerHome;