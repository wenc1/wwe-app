<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/roster', function(){
    $url = __DIR__.'/../project-data/project-data.json';
    $jsondata = file_get_contents($url);
    $data = json_decode($jsondata, true);
    $roster = $data['roster'];

    if(count($roster) === 0){
        return response(['error' => 'Not Found'], 404);
    } else {
        return response(['result' => $roster], 200);
    }
});

Route::get('/roster/{id}', function($id){
    $url = __DIR__. '/../project-data/project-data.json';
    $jsondata = file_get_contents($url);
    $data = json_decode($jsondata, true);
    $roster = $data['roster'];

    $result = -1;

    foreach ($roster as &$value) {
        if($id == $value['id']){
            $result = $value;
        }
    }

    if($result === -1){
        return response(['error' => 'Not Found'], 404);
    } else {
        return response(['result' => $result], 200);
    }
    
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
